import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getUsersDataRequest,
  putEditUsers,
} from "../../store/resourse/users/actions";
import { usersParamsSelector } from "../../store/resourse/users/selectors";
import { Button } from "../button/Button";
import style from "./modal-window.module.scss";

interface ModalWindowProps {
  age: number;
  name: string;
  id: string;
  closeChange: () => void;
}

const ModalWindow = ({ age, name, id, closeChange }: ModalWindowProps) => {
  const [nameValue, setNameValue] = useState(name);
  const [ageValue, setAgeValue] = useState(age.toString());
  const dispatch = useDispatch();
  const usersParams = useSelector(usersParamsSelector);

  const editChange = () => {
    if (nameValue !== "" && ageValue !== "") {
      dispatch(
        putEditUsers({
          age: ageValue,
          name: nameValue,
          userId: id,
        })
      );
      dispatch(
        getUsersDataRequest({
          limit: usersParams?.limit,
          order: usersParams?.order,
          page: usersParams?.page,
          sortBy: usersParams?.sortBy,
        })
      );
    }
  };

  return (
    <>
      <div className={style.wrapper__overlay} onClick={closeChange}></div>
      <div className={style.wrapper}>
        <div className={style.wrapper__header}>
          <Button
            text="Закрыть"
            clName={style.wrapper__close_button}
            btnClick={closeChange}
          />
        </div>
        <div className={style.wrapper__content}>
          <label>
            Редактировать имя:
            <input
              type="text"
              value={nameValue}
              onInput={(e) => setNameValue(e.currentTarget.value)}
            />
          </label>
          <label>
            Редактировать возраст:
            <input
              type="text"
              value={ageValue}
              onInput={(e) => setAgeValue(e.currentTarget.value)}
            />
          </label>
        </div>
        <div className={style.wrapper__buttons}>
          <Button
            text="Отправить"
            clName={style.wrapper__submit_button}
            btnClick={editChange}
          />
        </div>
      </div>
    </>
  );
};

export default ModalWindow;
