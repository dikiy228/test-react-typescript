interface ButtonProps {
  text: string;
  clName?: string;
  btnClick?: () => void;
}

export const Button = ({ text, btnClick, clName }: ButtonProps) => {
  return (
    <button
      className={clName}
      type="button"
      onClick={() => (btnClick ? btnClick() : null)}
    >
      {text}
    </button>
  );
};
