import { useDispatch } from "react-redux";
import { Button } from "../../../../components/button/Button";
import { deleteProducts } from "../../../../services/productsService";
import { ProductInterface } from "../../../../store/resourse/types";
import style from "./products-item.module.scss";

const ProductsItem = (item: ProductInterface) => {
  const dispatch = useDispatch();

  const deleteChange = () => {
    dispatch(deleteProducts(item.id));
  };

  return (
    <li className={style.wrapper}>
      {item.name}
      <Button
        text="Удалить"
        btnClick={deleteChange}
        clName={style.wrapper__delete_button}
      />
    </li>
  );
};

export default ProductsItem;
