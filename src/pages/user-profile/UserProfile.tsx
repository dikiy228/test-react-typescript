import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useParams } from "react-router-dom";
import { getUserProductsRequest } from "../../store/resourse/products/actions";
import {
  getUserProductsLoadingSelector,
  getUserProductsSelector,
  isDeletedUserProductsSelector,
} from "../../store/resourse/products/selectors";
import { getUserDataRequest } from "../../store/resourse/user/actions";
import {
  getUserLoadingSelector,
  getUserSelector,
} from "../../store/resourse/user/selectors";
import PageContentHeader from "../components/page-content-header/PageContentHeader";
import UserItem from "../components/user-item/UserItem";
import ProductsItem from "./components/product-item/ProductsItem";

import style from "./user-profile.module.scss";

const UserProfile = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const location = useLocation();
  const user = useSelector(getUserSelector);
  const userLoading = useSelector(getUserLoadingSelector);
  const products = useSelector(getUserProductsSelector);
  const productsLoading = useSelector(getUserProductsLoadingSelector);
  const isDeletedProducts = useSelector(isDeletedUserProductsSelector);

  useEffect(() => {
    if (params.id) {
      dispatch(
        getUserDataRequest({
          id: params.id,
        })
      );
      if (location.pathname.includes(params.id) && !isDeletedProducts?.includes(params.id)) {
        dispatch(getUserProductsRequest(params.id));
      }
    }
  }, [isDeletedProducts, location.pathname, params.id, products.length]);

  if (!user) return null;

  return (
    <div className={style.wrapper}>
      <div className="container">
        <PageContentHeader title="User Profile" />
        {userLoading && productsLoading ? (
          <div>Loading...</div>
        ) : (
          <>
            <div className={style.wrapper__user}>
              <UserItem
                name={user.name}
                age={user.age}
                avatar={user.avatar}
                company={user.company}
                id={user.id}
              />
            </div>
            {products.length > 0 && (
              <ul className={style.wrapper__products_list}>
                <h2>Продукты</h2>
                {products.map((item) => (
                  <ProductsItem
                    key={item.id}
                    name={item.name}
                    id={item.id}
                    userId={item.userId}
                  />
                ))}
              </ul>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default UserProfile;
