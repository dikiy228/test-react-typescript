import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  searchUsersLoadingSelector,
  searchUsersSelector,
} from "../../../../store/resourse/search-users/selectors";
import { getUsersDataRequest } from "../../../../store/resourse/users/actions";
import { getUsersLoadingSelector, getUsersSelector, usersParamsSelector } from "../../../../store/resourse/users/selectors";
import UserItem from "../../../components/user-item/UserItem";

const UserListItems = () => {
  const users = useSelector(getUsersSelector);
  const usersLoading = useSelector(getUsersLoadingSelector);
  const searchUsers = useSelector(searchUsersSelector);
  const searchUsersLoading = useSelector(searchUsersLoadingSelector);
  const usersParams = useSelector(usersParamsSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!users) {
      dispatch(
        getUsersDataRequest({
          limit: usersParams?.limit,
          order: usersParams?.order,
          page: usersParams?.page,
          sortBy: usersParams?.sortBy,
        })
      );
    }
  }, [users]);

  if (usersLoading || searchUsersLoading) return <div>Loading...</div>;

  if (!users && !searchUsers.length) return null;

  return (
    <>
      {!searchUsers.length && users &&
        users.map((item) => (
          <UserItem
            key={item.id}
            age={item.age}
            avatar={item.avatar}
            id={item.id}
            name={item.name}
          />
        ))}
      {searchUsers.length > 0 &&
        searchUsers.map((item) => (
          <UserItem
            key={item.id}
            age={item.age}
            avatar={item.avatar}
            id={item.id}
            name={item.name}
          />
        ))}
    </>
  );
};

export default UserListItems;
