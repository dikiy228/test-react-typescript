import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../../../components/button/Button";
import {
  getUsersDataRequest,
  usersParamsAction,
} from "../../../../store/resourse/users/actions";
import {
  paginationCountSelector,
  usersParamsSelector,
} from "../../../../store/resourse/users/selectors";
import style from "./pagination-items.module.scss";

const PaginationItems = () => {
  const count = useSelector(paginationCountSelector);
  const usersParams = useSelector(usersParamsSelector);
  const dispatch = useDispatch();

  const renderButtons = () => {
    const numbersButton: number[] = [];
    if (
      usersParams?.limit &&
      count &&
      count.length > 2 &&
      count.length > usersParams.limit
    ) {
      const math = Math.ceil(count.length / usersParams?.limit);
      for (let i = 1; i <= math; i++) {
        numbersButton.push(i);
      }
    }
    return numbersButton;
  };

  const getUsers = (page: number) => {
    if (usersParams?.page !== page) {
      dispatch(
        usersParamsAction({
          limit: usersParams?.limit,
          order: usersParams?.order,
          page: page,
          sortBy: usersParams?.sortBy,
        })
      );
      dispatch(
        getUsersDataRequest({
          limit: usersParams?.limit,
          order: usersParams?.order,
          page: page,
          sortBy: usersParams?.sortBy,
        })
      );
    }
  };

  if (
    (count && !count.length) ||
    (count && count.length < 2) ||
    usersParams?.limit === count?.length
  )
    return null;

  return (
    <div className={style.pagination_wrapper}>
      {renderButtons().map((item) => (
        <Button
          text={item.toString()}
          key={item + Math.random()}
          btnClick={() => getUsers(item)}
          clName={
            style.pagination_wrapper__button +
            " " +
            (usersParams?.page === item
              ? style.pagination_wrapper__button_active
              : null)
          }
        />
      ))}
    </div>
  );
};

export default PaginationItems;
