import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  resetSearchUsers,
  searchUsersDataRequest,
} from "../../store/resourse/search-users/actions";
import { searchUsersSelector } from "../../store/resourse/search-users/selectors";
import {
  getUsersDataRequest,
  usersParamsAction,
} from "../../store/resourse/users/actions";
import PageContentHeader from "../components/page-content-header/PageContentHeader";
import { Button } from "../../components/button/Button";
import style from "./user-list.module.scss";
import UserListItems from "./components/user-list-items/userListItems";
import {
  getUsersSelector,
  paginationCountSelector,
  usersParamsSelector,
} from "../../store/resourse/users/selectors";
import PaginationItems from "./components/pagination-items/PaginationItems";

const UserList = () => {
  const [searchValue, setSearchValue] = useState("");
  const dispatch = useDispatch();
  const users = useSelector(getUsersSelector);
  const searchUsers = useSelector(searchUsersSelector);
  const usersParams = useSelector(usersParamsSelector);
  const count = useSelector(paginationCountSelector);

  useEffect(() => {
    if (users && !users.length) {
      dispatch(
        getUsersDataRequest({
          limit: usersParams?.limit,
          order: usersParams?.order,
          page: usersParams?.page,
          sortBy: usersParams?.sortBy,
        })
      );
    }
  }, [users?.length]);

  const searchUser = useCallback(
    (name: string) => {
      if (name) {
        dispatch(
          searchUsersDataRequest({
            name: name,
          })
        );
      }
    },
    []
  );

  const resetSearch = () => {
    dispatch(resetSearchUsers());
    setSearchValue("");
  };

  const editParams = (
    editOrder?: string,
    editLimit?: number,
    editPage?: number,
    editSortBy?: string
  ) => {
    dispatch(
      usersParamsAction({
        limit: editLimit,
        order: editOrder,
        page: editPage,
        sortBy: editSortBy,
      })
    );
    dispatch(
      getUsersDataRequest({
        limit: editLimit,
        order: editOrder,
        page: editPage,
        sortBy: editSortBy,
      })
    );
  };

  const sortBy = (value: string, property: string) => {
    if (property === "age") {
      if (value === "descending") {
        editParams(
          "desc",
          usersParams?.limit,
          usersParams?.page,
          usersParams?.sortBy
        );
      } else if (value === "ascending") {
        editParams(
          "asc",
          usersParams?.limit,
          usersParams?.page,
          usersParams?.sortBy
        );
      }
    } else if (property === "count") {
      const limit = value === "all" ? count?.length : Number(value);
      editParams(
        usersParams?.order,
        limit,
        1,
        usersParams?.sortBy
      );
    }
  };

  if (users && !users.length && !searchUsers.length) return null;

  return (
    <div className={style.wrapper}>
      <div className="container">
        <PageContentHeader title="User List" />
        <label className={style.wrapper__sort}>
          <div>
            Сортировка по возрасту:
            <select
              className={style.wrapper__sort_select}
              onChange={(e) => sortBy(e.currentTarget.value, "age")}
              defaultValue={usersParams?.order}
            >
              <option value="descending">По убыванию</option>
              <option value="ascending">По возрастанию</option>
            </select>
          </div>
          <div>
            Сортировка по количеству:
            <select
              className={style.wrapper__sort_select}
              onChange={(e) => sortBy(e.currentTarget.value, "count")}
              defaultValue={usersParams?.order}
            >
              <option value="5">5</option>
              <option value="10">10</option>
              <option value="all">Все</option>
            </select>
          </div>
        </label>
        <div className={style.wrapper__background}>
          <div className={style.wrapper__search_item}>
            <input
              type="input"
              className={style.wrapper__search_input}
              value={searchValue}
              onInput={(e) => setSearchValue(e.currentTarget.value)}
            />
            <input
              type="submit"
              className={style.wrapper__search_submit}
              placeholder="Поиск"
              value="Поиск"
              disabled={searchValue === ""}
              onClick={() => searchUser(searchValue)}
            />
            {searchUsers.length > 0 && (
              <Button
                text="Сбросить"
                btnClick={resetSearch}
                clName={style.wrapper__search_reset}
              />
            )}
          </div>
          <ul className={style.wrapper__list}>
            <UserListItems />
          </ul>
          <PaginationItems />
        </div>
      </div>
    </div>
  );
};

export default UserList;
