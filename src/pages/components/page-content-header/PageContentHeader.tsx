import { BackButton } from "../back-button/BackButton";
import style from "./page-content-header.module.scss";

interface PageContentHeaderProps {
  title: string;
}

const PageContentHeader = ({ title }: PageContentHeaderProps) => {
  return (
    <div className={style.wrapper_header}>
      <div className={style.wrapper_header__items}>
        <BackButton />
        <div className={style.wrapper_header__title_wrapper}>{title}</div>
      </div>
    </div>
  );
};

export default PageContentHeader;
