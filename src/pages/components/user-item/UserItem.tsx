import React, { FC, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useLocation, useParams } from "react-router-dom";
import { Button } from "../../../components/button/Button";
import ModalWindow from "../../../components/modal-window/ModalWindow";
import { UserInterface } from "../../../store/resourse/types";
import { deleteUsers } from "../../../store/resourse/users/actions";
import style from "./user-item.module.scss";

const UserItem: FC<UserInterface> = ({ age, avatar, company, name, id }) => {
  const [openModal, setOpenModal] = useState(false);
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    if (openModal) {
      document.body.style.overflow = "hidden";
    } else if (!openModal) {
      document.body.style.overflow = "initial";
    }
  }, [openModal]);

  const deleteChange = () => {
    dispatch(deleteUsers(id));
  };

  return (
    <li className={style.wrapper}>
      <div className={style.wrapper__avatar}>
        <img src={avatar} alt="avatar" />
      </div>
      <div className={style.wrapper__info}>
        <Link to={`/user-profile/${id}`} className={style.wrapper__name}>
          {name}
        </Link>
        <p className={style.wrapper__age}>Age: {age}</p>
        {company && (
          <p className={style.wrapper__company}>
            <strong>{company.name}</strong>
            <strong>{company.date}</strong>
          </p>
        )}
      </div>
      {!location.pathname.includes(`user-profile/${id}`) && (
        <div className={style.wrapper__buttons}>
          <Button
            text="Редактировать"
            clName={style.wrapper__edit}
            btnClick={() => setOpenModal(true)}
          />
          <Button
            text="Удалить"
            clName={style.wrapper__delete}
            btnClick={deleteChange}
          />
        </div>
      )}
      {openModal && (
        <ModalWindow
          age={age}
          name={name}
          id={id}
          closeChange={() => setOpenModal(false)}
        />
      )}
    </li>
  );
};

export default UserItem;
