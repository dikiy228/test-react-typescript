import { useNavigate } from "react-router-dom";
import { Button } from "../../../components/button/Button";
import style from "./back-button.module.scss";

export const BackButton = () => {
  const navigate = useNavigate();
  return (
    <div className={style.wrapper}>
      <Button text="Назад" btnClick={() => navigate(-1)} clName={style.wrapper__back_button} />
    </div>
  );
};
