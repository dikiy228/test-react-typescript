import PageContentHeader from "../components/page-content-header/PageContentHeader";
import style from "./home.module.scss";

const Home = () => {
  return (
    <div>
      <div className={style.wrapper}>
        <div className="container">
          <PageContentHeader title="Home Page" />
          <div className={style.wrapper__content}>Home Page</div>
        </div>
      </div>
    </div>
  );
};

export default Home;
