import { ProductInterface } from './../../store/resourse/types';
import axios from 'axios';
import api from '../api';

export const getProducts = (id: string) => axios.get<ProductInterface[]>(api.URL_PRODUCTS + `/${id}`);

export const deleteProducts = (id: string) => axios.delete<ProductInterface[]>(api.URL_PRODUCTS + `/${id}`);