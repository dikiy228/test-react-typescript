let URL_API = "https://621c7b30768a4e1020ab3244.mockapi.io/api";

const common = {
    URL_USERS: `${URL_API}/users`,
    URL_PRODUCTS: `${URL_API}/products`,
}

let api = common;

export default api;

