import axios from "axios";
import { UserInterface } from "../../store/resourse/types";
import api from "../api";
import { EditParams, UsersParamsTypes } from "../types";

export const getUsers = (usersParams?: UsersParamsTypes) =>
  axios.get<UserInterface[]>(
    api.URL_USERS + `/${usersParams?.id ? usersParams.id : ""}`,
    {
      params: {
        name: usersParams?.name,
        page: usersParams?.page,
        limit: usersParams?.limit,
        sortBy: usersParams?.sortBy,
        order: usersParams?.order,
      },
    }
  );

export const editUsers = (editParams: EditParams) =>
  axios.put<UserInterface[]>(api.URL_USERS + `/${editParams?.userId ? editParams.userId : ""}`, {
    name: editParams.name,
    age: editParams.age,
  });

export const deleteUsers = (userId: string) =>
  axios.delete<UserInterface[]>(api.URL_USERS + `/${userId ? userId : ""}`);
