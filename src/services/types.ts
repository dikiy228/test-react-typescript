export interface UsersParamsTypes {
  id?: string;
  name?: string;
  page?: number;
  limit?: number;
  sortBy?: string;
  order?: string;
}

export interface EditParams {
  userId: string;
  name: string;
  age: string;
}
