import { ProductInterface } from "../types";
import { ActionTypes } from "./actionTypes";

export interface ProductsState {
  loading: boolean;
  products: ProductInterface[];
  error: string | null;
  isDeletedProducts?: string[];
}

export interface GetUserProductsDataRequest {
  type: ActionTypes.GET_USER_PRODUCTS_DATA;
  userId: string;
}

export interface GetUserProductsDataSuccessPayload {
  products: ProductInterface[];
}

export interface GetUserProductsDataFailPayload {
  error: string;
}

export type GetUserProductsDataSuccess = {
  type: ActionTypes.GET_USER_PRODUCTS_DATA_SUCCESS;
  payload: GetUserProductsDataSuccessPayload;
};

export type GetUserProductsDataFail = {
  type: ActionTypes.GET_USER_PRODUCTS_DATA_FAIL;
  payload: GetUserProductsDataFailPayload;
};

// Удаление продукта

export interface DeleteUserProduct {
  type: ActionTypes.DELETE_USERS_PRODUCTS;
  id: string;
}

export interface DeleteUserProductSuccessPayload {
  isDeletedProducts: string[];
}

export interface DeleteUsersProductSuccess {
  type: ActionTypes.DELETE_USERS_PRODUCTS_SUCCESS;
  payload: DeleteUserProductSuccessPayload;
}

export interface DeleteUserProductFailPayload {
  error: string;
}

export interface DeleteUsersProductFail {
  type: ActionTypes.DELETE_USERS_PRODUCTS_FAIL;
  payload: DeleteUserProductFailPayload;
}

export type ProductsActions =
  | GetUserProductsDataRequest
  | GetUserProductsDataSuccess
  | GetUserProductsDataFail
  | DeleteUserProduct
  | DeleteUsersProductSuccess
  | DeleteUsersProductFail;
