import { createSelector } from "reselect";
import { RootState } from "../../rootReducer";

const getUsersProductsLoading = (state: RootState) => state.products.loading;

const getUsersProducts = (state: RootState) => state.products.products;

const getUserProductsFail = (state: RootState) => state.products.error;

const deleteUserProductsSuccess = (state: RootState) => state.products.isDeletedProducts;

export const getUserProductsSelector = createSelector(
  getUsersProducts,
  (products) => products
);
export const getUserProductsLoadingSelector = createSelector(
  getUsersProductsLoading,
  (loading) => loading
);
export const getUserProductsFailSelector = createSelector(
  getUserProductsFail,
  (error) => error
);
export const isDeletedUserProductsSelector = createSelector(
  deleteUserProductsSuccess,
  (products) => products
);
