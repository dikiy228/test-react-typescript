export enum ActionTypes {
  GET_USER_PRODUCTS_DATA = "Get user products data",
  GET_USER_PRODUCTS_DATA_SUCCESS = "Get user products data success",
  GET_USER_PRODUCTS_DATA_FAIL = "Get user products data fail",

  DELETE_USERS_PRODUCTS = "Delete users products",
  DELETE_USERS_PRODUCTS_SUCCESS = "Delete users products succes",
  DELETE_USERS_PRODUCTS_FAIL = "Delete users products fail",
}
