import { all, call, put, select, takeLatest } from "redux-saga/effects";
import { deleteProducts, getProducts } from "../../../services/productsService";
import { ResponseGenerator } from "../../types";
import { UserInterface } from "../types";
import { getUserDataSuccess } from "../user/actions";
import {
  deleteUserProductsSuccess,
  getUserProductsFail,
  getUserProductsSuccess,
} from "./actions";
import { ActionTypes } from "./actionTypes";
import { isDeletedUserProductsSelector } from "./selectors";
import { DeleteUserProduct, GetUserProductsDataRequest } from "./types";

function* fetchUserProductsSaga(action: GetUserProductsDataRequest) {
  try {
    const response: ResponseGenerator = yield call(getProducts, action.userId);
    yield put(
      getUserProductsSuccess({
        products: [response.data],
      })
    );
  } catch (e) {
    const deletedItems: string[] = yield select(isDeletedUserProductsSelector);
    yield put(
      getUserProductsFail({
        error: e as string,
      })
    );
    yield put(
      deleteUserProductsSuccess({
        isDeletedProducts: deletedItems.concat(action.userId),
      })
    );
  }
}

function* fetchDeleteUserProductSaga(action: DeleteUserProduct) {
  try {
    yield call(deleteProducts, action.id);
    yield put(
      getUserDataSuccess({
        user: {} as UserInterface,
      })
    );
  } catch (e) {
    yield put(
      getUserProductsFail({
        error: e as string,
      })
    );
  }
}

function* userProductsSaga() {
  yield all([
    takeLatest(ActionTypes.GET_USER_PRODUCTS_DATA, fetchUserProductsSaga),
    takeLatest(ActionTypes.DELETE_USERS_PRODUCTS, fetchDeleteUserProductSaga),
  ]);
}

export default userProductsSaga;
