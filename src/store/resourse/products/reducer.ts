import { ActionTypes } from "./actionTypes";
import { ProductsActions, ProductsState } from "./types";

const initialState: ProductsState = {
  loading: false,
  products: [],
  error: null,
  isDeletedProducts: [],
};

const productsReducer = (state = initialState, action: ProductsActions) => {
  switch (action.type) {
    case ActionTypes.GET_USER_PRODUCTS_DATA:
      return {
        ...state,
        loading: true,
      };
    case ActionTypes.GET_USER_PRODUCTS_DATA_SUCCESS:
      return {
        ...state,
        products: action.payload.products,
        loading: false,
      };
    case ActionTypes.GET_USER_PRODUCTS_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    case ActionTypes.DELETE_USERS_PRODUCTS_SUCCESS:
      return {
        ...state,
        isDeletedProducts: action.payload.isDeletedProducts,
      };
    default:
      return {
          ...state
      };
  }
};

export default productsReducer;
