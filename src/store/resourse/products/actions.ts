import { ActionTypes } from "./actionTypes";
import { DeleteUserProduct, DeleteUserProductFailPayload, DeleteUserProductSuccessPayload, DeleteUsersProductFail, DeleteUsersProductSuccess, GetUserProductsDataFail, GetUserProductsDataFailPayload, GetUserProductsDataRequest, GetUserProductsDataSuccess, GetUserProductsDataSuccessPayload } from "./types";

export const getUserProductsRequest = (userId: string): GetUserProductsDataRequest => ({
  type: ActionTypes.GET_USER_PRODUCTS_DATA,
  userId: userId
});

export const getUserProductsSuccess = (
  payload: GetUserProductsDataSuccessPayload
): GetUserProductsDataSuccess => ({
  type: ActionTypes.GET_USER_PRODUCTS_DATA_SUCCESS,
  payload,
});

export const getUserProductsFail = (
  payload: GetUserProductsDataFailPayload
): GetUserProductsDataFail => ({
  type: ActionTypes.GET_USER_PRODUCTS_DATA_FAIL,
  payload,
});

// Удаление продукта

export const deleteUserProducts = (id: string): DeleteUserProduct => ({
  type: ActionTypes.DELETE_USERS_PRODUCTS,
  id: id, 
});

export const deleteUserProductsSuccess = (
  payload: DeleteUserProductSuccessPayload
): DeleteUsersProductSuccess => ({
  type: ActionTypes.DELETE_USERS_PRODUCTS_SUCCESS,
  payload,
});

export const deleteUserProductsFail = (
  payload: DeleteUserProductFailPayload
): DeleteUsersProductFail => ({
  type: ActionTypes.DELETE_USERS_PRODUCTS_FAIL,
  payload,
});
