export interface UserInterface {
  id: string;
  name: string;
  age: number;
  avatar: string;
  company?: {
    name: string;
    date: string;
  };
}

export interface ProductInterface {
  id: string;
  name: string;
  userId: string;
}
