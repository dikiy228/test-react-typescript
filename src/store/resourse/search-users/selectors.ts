import { createSelector } from "reselect";
import { RootState } from "../../rootReducer";

const searchUsersLoading = (state: RootState) => state.searchUsers.loading;

const searchUsers = (state: RootState) => state.searchUsers.users;

const searchUserFail = (state: RootState) => state.searchUsers.error;

export const searchUsersSelector = createSelector(searchUsers, (users) => users);
export const searchUsersLoadingSelector = createSelector(
  searchUsersLoading,
  (loading) => loading
);
export const searchUsersFailSelector = createSelector(
  searchUserFail,
  (error) => error
);
