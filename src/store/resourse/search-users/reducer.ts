import { SearchUsersActionTypes } from "./actionTypes";
import { SearchUsersActions, SearchUsersDataState } from "./types";


const initialState: SearchUsersDataState = {
  loading: false,
  users: [],
  error: null,
};

const searchUsersReducer = (state = initialState, action: SearchUsersActions) => {
  switch (action.type) {
    case SearchUsersActionTypes.SEARCH_USERS_DATA:
      return {
        ...state,
        loading: true,
      };
    case SearchUsersActionTypes.SEARCH_USERS_DATA_SUCCESS:
      return {
        ...state,
        users: action.payload.users,
        loading: false,
      };
    case SearchUsersActionTypes.SEARCH_USERS_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    case SearchUsersActionTypes.RESET_SEARCH_USERS:
      return {
        ...state,
        users: []
      };
    default:
      return {
          ...state
      };
  }
};

export default searchUsersReducer;
