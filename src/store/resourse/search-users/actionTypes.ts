export enum SearchUsersActionTypes {
  SEARCH_USERS_DATA = "Search users data",
  SEARCH_USERS_DATA_SUCCESS = "Search users data success",
  SEARCH_USERS_DATA_FAIL = "Search users data fail",
  RESET_SEARCH_USERS = "Reset search users"
}
