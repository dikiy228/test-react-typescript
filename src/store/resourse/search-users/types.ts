import { UsersParamsTypes } from "../../../services/types";
import { UserInterface } from "../types";
import { SearchUsersActionTypes } from "./actionTypes";

export interface SearchUsersDataState {
  loading: boolean;
  users: UserInterface[];
  error: string | null;
}

export interface SearchUsersDataRequest {
  type: SearchUsersActionTypes.SEARCH_USERS_DATA;
  params: UsersParamsTypes;
}

export interface SearchUsersDataSuccessPayload {
  users: UserInterface[];
}

export interface SearchUsersDataFailPayload {
  error: string;
}

export interface ResetSearchUsers {
  type: SearchUsersActionTypes.RESET_SEARCH_USERS;
}

export type SearchUsersDataSuccess = {
  type: SearchUsersActionTypes.SEARCH_USERS_DATA_SUCCESS;
  payload: SearchUsersDataSuccessPayload;
};

export type SearchUsersDataFail = {
  type: SearchUsersActionTypes.SEARCH_USERS_DATA_FAIL;
  payload: SearchUsersDataFailPayload;
};

export type SearchUsersActions =
  | SearchUsersDataRequest
  | SearchUsersDataSuccess
  | SearchUsersDataFail
  | ResetSearchUsers;
