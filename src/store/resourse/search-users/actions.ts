import { UsersParamsTypes } from "../../../services/types";
import { SearchUsersActionTypes } from "./actionTypes";
import { ResetSearchUsers, SearchUsersDataFail, SearchUsersDataFailPayload, SearchUsersDataRequest, SearchUsersDataSuccess, SearchUsersDataSuccessPayload } from "./types";

export const searchUsersDataRequest = (params: UsersParamsTypes): SearchUsersDataRequest => ({
  type: SearchUsersActionTypes.SEARCH_USERS_DATA,
  params: params
});

export const searchUsersDataSuccess = (
  payload: SearchUsersDataSuccessPayload
): SearchUsersDataSuccess => ({
  type: SearchUsersActionTypes.SEARCH_USERS_DATA_SUCCESS,
  payload,
});

export const searchUsersDataFail = (
  payload: SearchUsersDataFailPayload
): SearchUsersDataFail => ({
  type: SearchUsersActionTypes.SEARCH_USERS_DATA_FAIL,
  payload,
});

export const resetSearchUsers = (): ResetSearchUsers => ({
  type: SearchUsersActionTypes.RESET_SEARCH_USERS,
})
