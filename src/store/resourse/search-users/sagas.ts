import { all, call, put, takeLatest } from "redux-saga/effects";
import { getUsers } from "../../../services/usersService";
import { ResponseGenerator } from "../../types";
import { searchUsersDataFail, searchUsersDataSuccess } from "./actions";
import { SearchUsersActionTypes } from "./actionTypes";
import { SearchUsersDataRequest } from "./types";

function* fetchSearchUsersSaga(action: SearchUsersDataRequest) {
  try {
    const response: ResponseGenerator = yield call(getUsers, action.params);
    yield put(
      searchUsersDataSuccess({
        users: response.data,
      })
    );
  } catch (e) {
    yield put(
      searchUsersDataFail({
        error: e as string,
      })
    );
  }
}

function* searchUsersSaga() {
  yield all([
    takeLatest(SearchUsersActionTypes.SEARCH_USERS_DATA, fetchSearchUsersSaga),
  ]);
}

export default searchUsersSaga;
