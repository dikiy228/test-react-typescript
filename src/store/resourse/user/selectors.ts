import { createSelector } from "reselect";
import { RootState } from "../../rootReducer";

const getUsersLoading = (state: RootState) => state.user.loading;

const getUsers = (state: RootState) => state.user.user;

const getUserFail = (state: RootState) => state.user.error;

export const getUserSelector = createSelector(getUsers, (user) => user);
export const getUserLoadingSelector = createSelector(
  getUsersLoading,
  (loading) => loading
);
export const getUserFailSelector = createSelector(
  getUserFail,
  (error) => error
);