import { all, call, put, takeLatest } from "redux-saga/effects";
import { getUsers } from "../../../services/usersService";
import { ResponseGenerator } from "../../types";
import { getUserDataFail, getUserDataSuccess } from "./actions";
import { ActionTypes } from "./actionTypes";
import { GetUserDataRequest } from "./types";

function* fetchUserSaga(action: GetUserDataRequest) {
  try {
    const response: ResponseGenerator = yield call(getUsers, action.params);
    yield put(
      getUserDataSuccess({
        user: response.data,
      })
    );
  } catch (e) {
    yield put(
      getUserDataFail({
        error: e as string,
      })
    );
  }
}

function* userSaga() {
  yield all([
    takeLatest(ActionTypes.GET_USER_DATA, fetchUserSaga),
  ]);
}

export default userSaga;
