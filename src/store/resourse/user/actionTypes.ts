export enum ActionTypes {
  GET_USER_DATA = "Get user data",
  GET_USER_DATA_SUCCESS = "Get user data success",
  GET_USER_DATA_FAIL = "Get user data fail",
}
