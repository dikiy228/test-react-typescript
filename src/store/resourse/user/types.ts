import { ProductInterface } from './../types';
import { UsersParamsTypes } from "../../../services/types";
import { UserInterface } from "../types";
import { ActionTypes } from "./actionTypes";

export interface UserDataState {
  loading: boolean;
  user?: UserInterface;
  error: string | null;
  productsLoading?: boolean;
  products?: ProductInterface[];
  productsError?: string | null;
}

export interface GetUserDataRequest {
  type: ActionTypes.GET_USER_DATA;
  params: UsersParamsTypes;
}

export interface GetUserDataSuccessPayload {
  user: UserInterface;
}

export interface GetUserDataFailPayload {
  error: string;
}

export type GetUserDataSuccess = {
  type: ActionTypes.GET_USER_DATA_SUCCESS;
  payload: GetUserDataSuccessPayload;
};

export type GetUserDataFail = {
  type: ActionTypes.GET_USER_DATA_FAIL;
  payload: GetUserDataFailPayload;
};

export type UserActions =
  | GetUserDataRequest
  | GetUserDataSuccess
  | GetUserDataFail
