import { UsersParamsTypes } from "../../../services/types";
import { ActionTypes } from "./actionTypes";
import {
  GetUserDataFail,
  GetUserDataFailPayload,
  GetUserDataRequest,
  GetUserDataSuccess,
  GetUserDataSuccessPayload,
} from "./types";

export const getUserDataRequest = (params: UsersParamsTypes): GetUserDataRequest => ({
  type: ActionTypes.GET_USER_DATA,
  params: params,
});

export const getUserDataSuccess = (
  payload: GetUserDataSuccessPayload
): GetUserDataSuccess => ({
  type: ActionTypes.GET_USER_DATA_SUCCESS,
  payload,
});

export const getUserDataFail = (
  payload: GetUserDataFailPayload
): GetUserDataFail => ({
  type: ActionTypes.GET_USER_DATA_FAIL,
  payload,
});
