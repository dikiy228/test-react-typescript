import { ActionTypes } from "./actionTypes";
import { UserActions, UserDataState } from "./types";

const initialState: UserDataState = {
  loading: false,
  error: null,
};

const userReducer = (state = initialState, action: UserActions) => {
  switch (action.type) {
    case ActionTypes.GET_USER_DATA:
      return {
        ...state,
        loading: true,
      };
    case ActionTypes.GET_USER_DATA_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        loading: false,
      };
    case ActionTypes.GET_USER_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return {
          ...state
      };
  }
};

export default userReducer;
