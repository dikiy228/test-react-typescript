import { EditParams, UsersParamsTypes } from "../../../services/types";
import { UsersActionTypes } from "./actionTypes";
import {
  DeleteUsers,
  DeleteUsersFail,
  DeleteUsersFailPayload,
  EditUsers,
  EditUsersFail,
  EditUsersFailPayload,
  GetUsersDataFail,
  GetUsersDataFailPayload,
  GetUsersDataRequest,
  GetUsersDataSuccess,
  GetUsersDataSuccessPayload,
  UsersParams,
} from "./types";

export const getUsersDataRequest = (
  params: UsersParamsTypes
): GetUsersDataRequest => ({
  type: UsersActionTypes.GET_USERS_DATA,
  params: params,
});

export const getUsersDataSuccess = (
  payload: GetUsersDataSuccessPayload
): GetUsersDataSuccess => ({
  type: UsersActionTypes.GET_USERS_DATA_SUCCESS,
  payload,
});

export const getUsersDataFail = (
  payload: GetUsersDataFailPayload
): GetUsersDataFail => ({
  type: UsersActionTypes.GET_USERS_DATA_FAIL,
  payload,
});

// Опция для сортировака списка пользователей

export const usersParamsAction = (params: UsersParamsTypes): UsersParams => ({
  type: UsersActionTypes.USER_PARAMS,
  params: params,
});

// Редактирование пользователя

export const putEditUsers = (
  editParams: EditParams
): EditUsers => ({
  type: UsersActionTypes.EDIT_USERS,
  editParams: editParams,
});

export const putEditUsersFail = (
  payload: EditUsersFailPayload
): EditUsersFail => ({
  type: UsersActionTypes.EDIT_USERS_FAIL,
  payload,
});

// Удаление пользователя

export const deleteUsers = (userId: string): DeleteUsers => ({
  type: UsersActionTypes.DELETE_USERS,
  userId: userId,
});

export const deleteUsersFail = (
  payload: DeleteUsersFailPayload
): DeleteUsersFail => ({
  type: UsersActionTypes.DELETE_USERS_FAIL,
  payload,
});
