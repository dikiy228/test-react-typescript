import { EditParams, UsersParamsTypes } from "../../../services/types";
import { UserInterface } from "../types";
import { UsersActionTypes } from "./actionTypes";

export interface UsersDataState {
  loading: boolean;
  users?: UserInterface[];
  error: string | null;
  paginationLength?: number;
  paramsObj?: UsersParamsTypes;
  count?: UserInterface[];
  editUsersStatus?: boolean;
}

export interface GetUsersDataRequest {
  type: UsersActionTypes.GET_USERS_DATA;
  params: UsersParamsTypes;
}

export interface UsersParams {
  type: UsersActionTypes.USER_PARAMS;
  params: UsersParamsTypes;
}

export interface GetUsersDataSuccessPayload {
  users?: UserInterface[];
  count?: UserInterface[];
}

export interface GetUsersDataFailPayload {
  error: string;
}

export type GetUsersDataSuccess = {
  type: UsersActionTypes.GET_USERS_DATA_SUCCESS;
  payload: GetUsersDataSuccessPayload;
};

export type GetUsersDataFail = {
  type: UsersActionTypes.GET_USERS_DATA_FAIL;
  payload: GetUsersDataFailPayload;
};

// Редактирование пользователя

export interface EditUsers {
  type: UsersActionTypes.EDIT_USERS;
  editParams: EditParams;
}

export interface EditUsersFailPayload {
  error: string;
}

export interface EditUsersFail {
  type: UsersActionTypes.EDIT_USERS_FAIL;
  payload: EditUsersFailPayload;
}

// Удаление пользователя

export interface DeleteUsers {
  type: UsersActionTypes.DELETE_USERS;
  userId: string;
}

export interface DeleteUsersFailPayload {
  error: string;
}

export interface DeleteUsersFail {
  type: UsersActionTypes.DELETE_USERS_FAIL;
  payload: DeleteUsersFailPayload;
}

export type UsersActions =
  | GetUsersDataRequest
  | GetUsersDataSuccess
  | GetUsersDataFail
  | UsersParams
  | EditUsers
  | EditUsersFail
  | DeleteUsers
  | DeleteUsersFail;
