import { createSelector } from "reselect";
import { RootState } from "../../rootReducer";

const getUsersLoading = (state: RootState) => state.users.loading;

const getUsers = (state: RootState) => state.users.users;

const getUserFail = (state: RootState) => state.users.error;

const userParams = (state: RootState) => state.users.paramsObj;

const paginationCount = (state: RootState) => state.users.count;

export const getUsersSelector = createSelector(getUsers, (users) => users);
export const getUsersLoadingSelector = createSelector(
  getUsersLoading,
  (loading) => loading
);
export const getUsersFailSelector = createSelector(
  getUserFail,
  (error) => error
);

export const usersParamsSelector = createSelector(userParams, (option) => option);

export const paginationCountSelector = createSelector(paginationCount, (count) => count);
