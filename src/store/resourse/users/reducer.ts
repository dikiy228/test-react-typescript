import { UsersActionTypes } from "./actionTypes";
import { UsersActions, UsersDataState } from "./types";

const initialState: UsersDataState = {
  loading: false,
  users: [],
  error: null,
  paramsObj: {
    limit: 5,
    order: "desc",
    page: 1,
    sortBy: "age"
  },
};

const usersReducer = (state = initialState, action: UsersActions) => {
  switch (action.type) {
    // Запрос пользователей
    case UsersActionTypes.GET_USERS_DATA:
      return {
        ...state,
        loading: true,
      };
    case UsersActionTypes.GET_USERS_DATA_SUCCESS:
      return {
        ...state,
        users: action.payload.users,
        count: action.payload.count,
        loading: false,
      };
    case UsersActionTypes.GET_USERS_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };

    // Опция для сортировака списка пользователей
    case UsersActionTypes.USER_PARAMS:
      return {
        ...state,
        paramsObj: action.params,
      };
    default:
      return {
        ...state,
      };
  }
};

export default usersReducer;
