export enum UsersActionTypes {
  GET_USERS_DATA = "Get users data",
  GET_USERS_DATA_SUCCESS = "Get users data success",
  GET_USERS_DATA_FAIL = "Get users data fail",

  USER_PARAMS = "Users params",

  EDIT_USERS = "Edit users",
  EDIT_USERS_FAIL = "Edit users fail",

  DELETE_USERS = "Delete users",
  DELETE_USERS_FAIL = "Delete users fail",
}
