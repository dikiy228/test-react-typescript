import { all, call, put, select, takeLatest } from "redux-saga/effects";
import {
  deleteUsers,
  editUsers,
  getUsers,
} from "../../../services/usersService";
import { ResponseGenerator } from "../../types";
import { UserInterface } from "../types";
import { getUsersDataFail, getUsersDataSuccess } from "./actions";
import { UsersActionTypes } from "./actionTypes";
import { paginationCountSelector } from "./selectors";
import { DeleteUsers, EditUsers, GetUsersDataRequest } from "./types";

function* fetchUsersSaga(action: GetUsersDataRequest) {
  try {
    const count: UserInterface[] = yield select(paginationCountSelector);
    if (!count) {
      const response: ResponseGenerator = yield call(getUsers, {});
      yield put(
        getUsersDataSuccess({
          count: response.data,
        })
      );
    }
    const response: ResponseGenerator = yield call(getUsers, action.params);
    yield put(
      getUsersDataSuccess({
        users: response.data,
        count: count,
      })
    );
  } catch (e) {
    yield put(
      getUsersDataFail({
        error: e as string,
      })
    );
  }
}

function* fetchEditUsersSaga(action: EditUsers) {
  try {
    yield call(editUsers, action.editParams);
    yield put(
      getUsersDataSuccess({
        users: [],
      })
    );
  } catch (e) {
    yield put(
      getUsersDataFail({
        error: e as string,
      })
    );
  }
}

function* fetchDeleteUsersSaga(action: DeleteUsers) {
  try {
    yield call(deleteUsers, action.userId);
    yield put(
      getUsersDataSuccess({
        users: [],
      })
    );
  } catch (e) {
    yield put(
      getUsersDataFail({
        error: e as string,
      })
    );
  }
}

function* usersSaga() {
  yield all([
    takeLatest(UsersActionTypes.GET_USERS_DATA, fetchUsersSaga),
    takeLatest(UsersActionTypes.EDIT_USERS, fetchEditUsersSaga),
    takeLatest(UsersActionTypes.DELETE_USERS, fetchDeleteUsersSaga),
  ]);
}

export default usersSaga;
