import { all, fork } from "redux-saga/effects";

import usersSaga from "./resourse/users/sagas";
import userSaga from "./resourse/user/sagas";
import searchUsersSaga from "./resourse/search-users/sagas";
import userProductsSaga from "./resourse/products/sagas";

export function* rootSaga() {
  yield all([
    fork(usersSaga), 
    fork(userSaga),
    fork(searchUsersSaga),
    fork(userProductsSaga),
  ]);
}