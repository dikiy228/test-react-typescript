import { combineReducers } from "redux";
import productsReducer from "./resourse/products/reducer";
import searchUsersReducer from "./resourse/search-users/reducer";
import userReducer from "./resourse/user/reducer";
import usersReducer from "./resourse/users/reducer";


const rootReducer = combineReducers({
    users: usersReducer,
    user: userReducer,
    searchUsers: searchUsersReducer,
    products: productsReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;