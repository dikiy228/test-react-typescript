import React from "react";
import { Link, Route, Routes } from "react-router-dom";
import style from "./App.module.scss";
import Home from "./pages/home/Home";
import UserList from "./pages/user-list/UserList";
import UserProfile from "./pages/user-profile/UserProfile";

function App(): JSX.Element {
  return (
    <div className={style.wrapper}>
      <header className={style.wrapper__header}>
        <div className="container">
          <nav className={style.wrapper__header_nav}>
            <Link to="/">Home</Link>
            <Link to="/user-list">UserList</Link>
          </nav>
        </div>
      </header>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/user-list" element={<UserList />} />
        <Route path="/user-profile/:id" element={<UserProfile />} />
      </Routes>
    </div>
  );
}

export default App;
